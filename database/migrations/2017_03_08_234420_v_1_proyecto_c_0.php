<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class V1ProyectoC0 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->string('duracion');
            $table->enum('estado', ['no iniciado','en desarrollo', 'terminado'])->default('no iniciado');
            $table->integer('tarea_id')->unsigned();
            $table->foreign('tarea_id')
                ->references('id')->on('tarea')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyecto');
    }
}
