<?php

use Illuminate\Database\Seeder;

class UsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuario')->delete();
        DB::table('usuario')->insert(
            array(
                array(
                    'id' => 1,
                    'email' => 'edic.escorpio@gmail.com',
                    'nombre' => 'Edinson Junior',
                    'apellido' => 'Cruz Cruz',
                    'genero' => 'masculino',
                    'edad' => '22',
                ),
                array(
                    'id' => 2,
                    'email' => 'junior.cruz@janaq.com',
                    'nombre' => 'Edinson Junior',
                    'apellido' => 'Cruz Cruz',
                    'genero' => 'masculino',
                    'edad' => '22',
                )
            )
        );
    }
}
