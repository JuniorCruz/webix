<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Usuario;
use Laracasts\Flash\Flash;
use Auth;
use Socialite;

class SocialController extends Controller
{
    public function getSocialAuth()
    {
        if(!config("services.google")) abort('404');

        return Socialite::driver('google')->redirect();
    }


    public function getSocialAuthCallback()
    {

        if($googleUser = Socialite::driver('google')->user())
        {
            $usuario = Usuario::select()->where('email', '=', $googleUser->getEmail())->first();
            if ($usuario)
            {
                Auth::login($usuario);
                return redirect()->action('AccessController@index');
            }
            else
            {
                Flash::error('Error: no se encuentra registrado.');
                return redirect('login');
            }

            return redirect('home');
        }
        else{
            return '¡¡¡Algo fue mal!!!';
        }
    }
}
