<?php

namespace App\Http\Controllers;

use App\Models\Proyecto;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index(){

        return view('project.index');
    }

    public function create()
    {
        return view('project.create');
    }

    public function store()
    {
        $data = request()->all();
        Proyecto::create($data);
        //Flash::success('Bebida de Evento creado con Exito');
        return redirect()->route('project.index');
    }

    public function edit($id)
    {
        $model = Proyecto::findOrFail($id);
        return view('project.edit', compact(
            'model'
        ));
    }

    public function update($id)
    {
        $data = request()->all();
        $model = Proyecto::findOrFail($id);
        $model->fill($data);
        $model->save();
        //Flash::success('Bebida de Evento Actualizado');
        return redirect()->route('project.index');
    }

    public function destroy()
    {
        $request = request()->all();
        $model = Proyecto::findOrFail($request['id']);
        $model->delete();
        return response()->json([
            'status' => 200
        ]);
    }
    public function state($id){
        $model = Proyecto::findOrFail($id);
        $model->estado= $model->estado == 'activo' ? 'inactivo' : 'activo';
        $model->save();
        //Flash::success('Estado de Pregunta Actualizada!');
        return redirect()->back();
    }
}
