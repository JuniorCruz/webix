<?php

namespace App\Http\Controllers;

use App\Models\Tarea;
use Illuminate\Http\Request;
use DB;

class TaskController extends Controller
{
    public function index()
    {
        $model=DB::table('tarea as t')
            ->join('usuario as u', 't.usuario_id', '=', 'u.id')
            ->select('t.descripcion', 't.tipo', 't.duracion', 't.estado', 'u.nombre');

        return view('task.index', compact(
            'model'
        ));
    }

    public function create()
    {
        return view('task.create');
        //dd("dsdfdgdsfgsfg");
    }

    public function store()
    {
        dd('asldsldkdlmsdlkvnlskdmlkm');
        /*$data = request()->all();
        dd($data);*/
        //Tarea::create($data);
        //Flash::success('Bebida de Evento creado con Exito');
        //return redirect()->route('task.index');
    }

    /*public function edit($id)
    {
        $model = BebidaEvento::findOrFail($id);
        return view('admin.drink_event.edit', compact(
            'model'
        ));
    }*/

    public function update($id)
    {
        $data = request()->all();
        $model = Tarea::findOrFail($id);
        $model->fill($data);
        $model->save();
        //Flash::success('Bebida de Evento Actualizado');
        return redirect()->route('task.index');
    }

    public function destroy()
    {
        $request = request()->all();
        $model = Tarea::findOrFail($request['id']);
        $model->delete();
        return response()->json([
            'status' => 200
        ]);
    }
    public function state($id){
        $model = Tarea::findOrFail($id);
        $model->estado= $model->estado == 'activo' ? 'inactivo' : 'activo';
        $model->save();
        //Flash::success('Estado de Pregunta Actualizada!');
        return redirect()->back();
    }
}
