<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/*use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;*/
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use Notifiable;

    protected $table = 'usuario';

    protected $fillable = [
        'email',
        'nombre',
        'apellido',
        'genero',
        'edad',
    ];

    public function tarea()
    {
        return$this->hasOne(Tarea::class, 'usuario_id')->where('deleted_at',null);
    }
}
