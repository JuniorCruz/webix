<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['web'] ], function () {

    Route::get('lang/{lang}', function ($lang) {
        session(['lang' => $lang]);
        return \Redirect::back();
    })->where([
        'lang' => 'en|es|it|ja|fr'
    ]);
});

Auth::routes();

Route::get('/home', 'HomeController@index');



/*Route::get('/',['as' => 'login','uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/',['as' => 'login','uses' => 'Auth\LoginController@login']);*/
Route::get('/logout', ['as' => 'logout','uses' => 'Auth\LoginController@logout']);

Route::get('social/google', ['as' => 'social.google', 'uses' => 'SocialController@getSocialAuth']);
Route::get('social/google/callback', ['as' => 'social.google.callback', 'uses' => 'SocialController@getSocialAuthCallback']);
Route::get('/acceso',['as' => 'access.index', 'uses' => 'AccessController@index']);
//Route::get('/project',['as' => 'project.index', 'uses' => 'ProjectController@index']);



Route::group(['prefix' => 'project'],function() {
    Route::get('/', ['as' => 'project.index', 'uses' => 'ProjectController@index']);
    Route::get('/create',['as' => 'project.create', 'uses' => 'ProjectController@create']);
    Route::post('/create',['as' => 'project.create', 'uses' => 'ProjectController@store']);
    Route::get('/edit/{id}',['as' => 'project.edit', 'uses' => 'ProjectController@edit']);
    Route::post('/edit/{id}',['as' => 'project.edit', 'uses' => 'ProjectController@update']);
    Route::get('/state/{id}',['as' => 'project.state', 'uses' => 'ProjectController@state']);
    Route::post('/delete',['as' => 'project.delete', 'uses' => 'ProjectController@destroy']);
});

Route::group(['prefix' => 'task'],function() {
    Route::get('/', ['as' => 'task.index', 'uses' => 'TaskController@index']);
    Route::get('/create',['as' => 'task.create', 'uses' => 'TaskController@create']);
    Route::post('/create',['as' => 'task.create', 'uses' => 'TaskController@store']);
    Route::get('/edit/{id}',['as' => 'task.edit', 'uses' => 'TaskController@edit']);
    Route::post('/edit/{id}',['as' => 'task.edit', 'uses' => 'TaskController@update']);
    Route::get('/state/{id}',['as' => 'task.state', 'uses' => 'TaskController@state']);
    Route::post('/delete',['as' => 'task.delete', 'uses' => 'TaskController@destroy']);
});
