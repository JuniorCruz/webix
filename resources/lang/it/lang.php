<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'number' => [
        'first'             => 'Prima',
        'second'            => 'Secondo',
        'third'             => 'Terzo',
        'fourth'            => 'Quarto',
        'fifth'             => 'Quinto',
        'sixth'             => 'Sesto',
    ],

    'register'             => 'Record',
    'name'                 => 'Nome',
    'email'                => 'Indirizzo E-mail',
    'password'             => 'Password',
    'confirm_password'     => 'Confermare la Password',
    'user_name'            => 'Nome Utente',
    'reset'                => 'Riavvio',
    'login_google'         => 'Acceso con Google',
    'login'                => 'Acceso',
    'languages'            => 'Linguaggi',
    'access'               => 'Hai inserito correttamente',
    'logout'               => 'esci',

];
