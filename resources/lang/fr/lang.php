<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'number' => [
        'first'             => 'Première',
        'second'            => 'Deuxième',
        'third'             => 'Troisième',
        'fourth'            => 'Quatrième',
        'fifth'             => 'Cinquième',
        'sixth'             => 'Sixième',
    ],

    'register'             => 'Compte rendu',
    'name'                 => 'Nom',
    'email'                => 'Adresse E-mail',
    'password'             => 'Mot de Passe',
    'confirm_password'     => 'Confirmer Mot de Passe',
    'user_name'            => "Nom d'utilisateur",
    'reset'                => 'Reboot',
    'login_google'         => 'Connectez-vous avec Google',
    'login'                => 'Connectez-vous',
    'languages'            => 'Langues',
    'access'               => 'Vous avez entré correctement',
    'logout'               => 'Déconnexion',

];
