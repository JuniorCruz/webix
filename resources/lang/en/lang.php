<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'number' => [
        'first'             => 'First',
        'second'            => 'Second',
        'third'             => 'Third',
        'fourth'            => 'Fourth',
        'fifth'             => 'Fifth',
        'sixth'             => 'Sixth',
    ],

    'register'             => 'Register',
    'name'                 => 'Name',
    'email'                => 'E-mail Addres',
    'password'             => 'Password',
    'confirm_password'     => 'Confirm Password',
    'user_name'            => 'User Name',
    'reset'                => 'Reset',
    'login_google'         => 'Login Google',
    'login'                => 'Login',
    'languages'            => 'Languages',
    'access'               => 'You entered correctly',
    'logout'               => 'Logout',

];
