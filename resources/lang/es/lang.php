<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'number' => [
        'first'             => 'Primer',
        'second'            => 'Segundo',
        'third'             => 'Tercer',
        'fourth'            => 'Cuarto',
        'fifth'             => 'Quinto',
        'sixth'             => 'Sexto',
    ],

    'register'             => 'Registrar',
    'name'                 => 'Nombre',
    'email'                => 'Dirección de E-mail',
    'password'             => 'Contraseña',
    'confirm_password'     => 'Confirmar Contraseña',
    'user_name'            => 'Nombre de Usuario',
    'reset'                => 'Reiniciar',
    'login_google'         => 'Iniciar Sesión con Google',
    'login'                => 'Iniciar Sesión',
    'languages'            => 'Idiomas',
    'access'               => 'Usted a ingresado correctamente',
    'logout'               => 'Cerrar Sesión',

];
