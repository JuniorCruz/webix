<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'number' => [
        'first'             => '最初の',
        'second'            => '2番目の',
        'third'             => '3番目の',
        'fourth'            => '4番目の',
        'fifth'             => '5番目の',
        'sixth'             => '6番目の',
    ],

    'register'             => 'レコード',
    'name'                 => '名前',
    'email'                => 'Eメールアドレス',
    'password'             => 'パスワード',
    'confirm_password'     => 'パスワードを確認',
    'user_name'            => 'ユーザー名',
    'reset'                => 'リブート',
    'login_google'         => 'グーグルでログイン',
    'login'                => 'ログイン',
    'languages'            => '言語',
    'access'               => 'あなたはに正しく入力され',
    'logout'               => 'ログアウト',

];
