<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Acceso</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('webix/webix/codebase/webix.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('webix/webix/codebase/webix.js') }}"></script>
    <form method="post">
        <script>
            function logout(){
                window.location.href = "{{ route('logout') }}";
            }
            function acceso(){
                window.location.href = "{{ route('access.index') }}";
            }
            function project(){
                window.location.href = "{{ route('project.index') }}";
            }

            function addTask()
            {
                payLoad.descripcion =$$("descripcion").getValue();
                payLoad.tipo =$$("tipo").getValue();
                payLoad.duracion =$$("duracion").getValue();
                payLoad.estado =$$("estado").getValue();



                if(payLoad.descripcion == '')
                {
                    webix.message({type:"error", text: "Por favor ingrese Descripción"});
                }
                else
                {
                    webix.message({text: "Por favor espere..."});
                    webix.ajax().post( "{{ route('project.create') }}" , payLoad, callbackMethod());

                }
            }
            function callbackMethod()
            {
                window.location.href = "{{ redirect()->action('ProjectController@create') }}";
            }

            var payLoad = {
                descripcion: "",
                tipo : "",
                duracion : "",
                estado : ""
            };
            var estado= [
                {id:"no iniciado", value: "no iniciado"},
                {id:"en desarrollo", value: "en desarrollo"},
                {id:"terminado", value: "terminado"}
            ];
            var tipo= [
                {id:"fase", value: "fase"},
                {id:"tarea", value: "tarea"}
            ];

            var filmset = [
                { id:1, title:"The Shawshank Redemption", year:1994},
                { id:2, title:"The Godfather", year:1972},
                { id:3, title:"The Godfather: Part II", year:1974},
                { id:4, title:"The Good, the Bad and the Ugly", year:1966},
                { id:5, title:"My Fair Lady", year:1964},
                { id:6, title:"12 Angry Men", year:1957}
            ];

            webix.ui({
                id: "panel",
                rows: [
                    {
                        view:"toolbar", elements:[
                            {   view: "toolbar", padding:3, elements: [
                                    {view: "button", type: "icon", icon: "bars",
                                        width: 37, align: "left", css: "app_button", click: function(){
                                        $$("$sidebar1").toggle()
                                    }
                                    },
                                    { view: "label", label: "Admin"},
                                    {},
                                    { view: "button", type: "icon", width: 45, css: "app_button", icon: "envelope-o",  badge:4},
                                    { view: "button", type: "icon", width: 45, css: "app_button", icon: "cog",  badge:10},
                                    { view:"button", value:"{{ trans('lang.logout') }}", click: "logout", width:120 }
                                ]
                            }
                        ]
                    },
                    {cols: [
                        {

                            width: 200,

                            body:{
                                view:"list",select: true,
                                borderless:true,
                                scroll: false,
                                template: "<span class='webix_icon fa-#icon#'></span> #value#",
                                data:[
                                    "<a href='/acceso'> <i class='fa fa-user'> <p>Inicio</p> </i> </a>",
                                    "<a href='/project'> <i class='fa fa-user'> <p>Proyectos</p> </i> </a>",
                                    "<a href='/task'> <i class='fa fa-user'> <p>Tareas</p> </i> </a>",
                                    /*{id: 1, value: "Customers", icon: "user", click:"routes"},
                                    {id: 2, value: "Products", icon: "cube", click:"task"},
                                    {id: 3, value: "Reports", icon: "line-chart"},
                                    {id: 4, value: "Archives", icon: "database"},
                                    {id: 5, value: "Settings", icon: "cog"}*/
                                ]
                            }
                        },
                        //{view: "resizer"},
                        {rows: [
                            {type:"header", template:"Agregar Nuevo Proyecto"},
                            {
                                //type:"line",
                                rows: [

                                    { cols:[
                                        {template:" "},
                                        {view:"form", id:"myform", name:"myform", width: 600,  elements:[
                                            {view:"text", id:"descripcion", name:"descripcion", placeholder:"Descripción", width:180, label:"Descripción", labelWidth:200, inputWidth:450},
                                            {view: "select", id:"tipo", name:"tipo", options: tipo, inputWidth: 180, label:"Tipo", labelWidth:200, inputWidth:350},
                                            {view:"counter", id:"duracion", name:"duracion", min:1, max: 24, value: 1, step:1, label:"Duracion(horas)",labelWidth:200, inputWidth:350},
                                            {view: "select", id:"estado", name:"estado", options: estado, inputWidth: 180, label:"Estado",labelWidth:200, inputWidth:350},
                                            {view:"button", type:"form", value:"Guardar", click: "addTask"},
                                            {view:"button", type:"form", value:"Cancelar", click:'project'},
                                            {}
                                        ]},
                                        {template:" "}
                                        ]
                                    }

                                ]
                            },
                            {template:" "}
                        ]}
                    ]
                    }
                ]
            });

            /* Funciones del mantendor */

            function add_row(){
                $$("mylist").add({
                    title: $$("myform").getValues().title,
                    year: $$("myform").getValues().year

                });
                webix.alert("Ha sido agregado correctamente...!");
            }

            $$("mylist").attachEvent("onAfterSelect", function(id){
                $$("myform").setValues({
                    title: $$("mylist").getItem(id).title,
                    year: $$("mylist").getItem(id).year
                });
            });

            function update_row() {
                var sel = $$("mylist").getSelectedId();
                if(!sel) return;

                var value1 = $$("myform").getValues().title;
                var value2 = $$("myform").getValues().year;


                var item = $$("mylist").getItem(sel); //selected item object
                item.title = value1;
                item.year = value2;
                $$("mylist").updateItem(sel, item);

                webix.alert("Ha sido actualizado correctamente...!");
            }

            function delete_row() {
                var id = $$("mylist").getSelectedId();

                webix.confirm({
                    title: "Eliminar",// the text of the box header
                    text: "¿Está seguro de que desea eliminar el elemento seleccionado?",
                    callback: function(result) {
                        if (result) {
                            $$("mylist").remove(id);
                            webix.alert("Ha sido Eliminado correctamente...!");
                        }
                    }
                });
            }
            /* ----------------------------------------------------------------------------- */
        </script>
    </form>


</head>
<body>

</body>
</html>

