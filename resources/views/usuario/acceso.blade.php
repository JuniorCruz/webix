<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Acceso</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('webix/webix/codebase/webix.css') }}" rel="stylesheet">

    <style>
        body{
            background: #faf6ed;
        }
        #areaA, #areaB{
            margin: 50px;
            width:700px; height:110px;
        }
    </style>

    <!-- Scripts -->
    <script src="{{ asset('webix/webix/codebase/webix.js') }}"></script>
    <form action="">
        <script>
            function logout(){
                window.location.href = "{{ route('logout') }}";
            }

            function routes(){
                window.location.href = "{{ route('project.index') }}";
            }
            function task(){
                window.location.href = "{{ route('task.index') }}";
            }


            var project = [
                {id:"root", value:"Proyectos", open:true, data:[
                    { id:"1", open:true, value:"Proyecto 1", click: "rutas"}
                ]}

            ];


            webix.ui({
                id: "panel",
                rows: [
                    {
                        view:"toolbar", elements:[
                            {   view: "toolbar", padding:3, elements: [
                                    {view: "icon", icon: "bars"},
                                    { view: "label", label: "Admin"},
                                    {},
                                    { view: "button", type: "icon", width: 45, css: "app_button", icon: "envelope-o",  badge:4},
                                    { view: "button", type: "icon", width: 45, css: "app_button", icon: "cog",  badge:10},
                                {
                                    container:"areaA",
                                    view:"menu",
                                    data:[
                                        { id:"1",value:"Configuración...", submenu:[
                                            {value:"Logout", click:"logout"}
                                        ]}
                                    ]

                                },
                                    { view:"button", value:"{{ trans('lang.logout') }}", click: "logout", width:120 },
                                    { view:"button", value:"project", click: "routes", width:120 }
                                ]
                            }
                        ]
                    },
                    {cols: [
                        {
                            width: 200,

                            body:{
                                view:"list",select: true,
                                borderless:true,
                                scroll: false,
                                template: "<span class='webix_icon fa-#icon#'></span> #value#",
                                data:[
                                    "<a href='/acceso'> <i class='fa fa-user'> <p>Inicio</p> </i> </a>",
                                    "<a href='/project'> <i class='fa fa-user'> <p>Proyectos</p> </i> </a>",
                                    "<a href='/task'> <i class='fa fa-user'> <p>Tareas</p> </i> </a>",
                                    /*{id: 1, value: "Customers", icon: "user", click:"routes"},
                                     {id: 2, value: "Products", icon: "cube", click:"task"},
                                     {id: 3, value: "Reports", icon: "line-chart"},
                                     {id: 4, value: "Archives", icon: "database"},
                                     {id: 5, value: "Settings", icon: "cog"}*/
                                ]
                            }
                        },
                        //{view: "resizer"},
                        {rows: [
                            {
                                view:"datatable",
                                responsive:"panel",
                                columns:[
                                    { id:"title",   header:"Film title",  width: 698},
                                    { id:"year",    header:"Year",      width:100},
                                    { id:"votes",   header:"Votes",     width:100},
                                    { id:"aaa",     header:"aaa",       width:100},
                                    { id:"category",header:"Category",  width:100, editor:"select", options:["","Crime", "Thriller"]}
                                ],
                                data:[
                                    { id:1, title:"The Shawshank Redemption", year:1994, votes:678790, aaa:111, category:""},
                                    { id:2, title:"The Godfather",            year:1972, votes:511495, aaa:111, category:""},
                                    { id:3, title:"The Godfather: Part II",   year:1974, votes:319352, aaa:111, category:""},
                                    { id:4, title:"Pulp fiction",             year:1994, votes:533848, aaa:111, category:""},
                                    { id:5, title:"Moonligth",                year:2016, votes:863349, aaa:111, category:""}

                                ],
                                editable:true,
                                select:"row",
                                multiselect:true
                            }
                        ]}
                    ]
                    }
                ]
            });
            /* Funciones del mantendor */

            function add_row(){
                $$("mylist").add({
                    title: $$("myform").getValues().title,
                    year: $$("myform").getValues().year

                });
                webix.alert("Ha sido agregado correctamente...!");
            }

            $$("mylist").attachEvent("onAfterSelect", function(id){
                $$("myform").setValues({
                    title: $$("mylist").getItem(id).title,
                    year: $$("mylist").getItem(id).year
                });
            });

            function update_row() {
                var sel = $$("mylist").getSelectedId();
                if(!sel) return;

                var value1 = $$("myform").getValues().title;
                var value2 = $$("myform").getValues().year;


                var item = $$("mylist").getItem(sel); //selected item object
                item.title = value1;
                item.year = value2;
                $$("mylist").updateItem(sel, item);

                webix.alert("Ha sido actualizado correctamente...!");
            }

            function delete_row() {
                var id = $$("mylist").getSelectedId();

                webix.confirm({
                    title: "Eliminar",// the text of the box header
                    text: "¿Está seguro de que desea eliminar el elemento seleccionado?",
                    callback: function(result) {
                        if (result) {
                            $$("mylist").remove(id);
                            webix.alert("Ha sido Eliminado correctamente...!");
                        }
                    }
                });
            }
            /* ----------------------------------------------------------------------------- */
        </script>
    </form>


</head>
<body>

</body>
</html>

