@extends('layouts.app')

@section('content')
    <body>
        <form method="post">
            <script>
                function validateForm() {
                    payLoad.userName =$$("theUserName").getValue();
                    payLoad.password =$$("thePassword").getValue();
                    payLoad.loginClicked = "Y";
                    payLoad.resetClicked = "N";

                    var localMessage = '';

                    if(payLoad.userName == '')
                    {
                        webix.message({type:"error", text: "Por favor ingrese user name"});
                    }
                    else if(payLoad.password == '')
                    {
                        webix.message({type:"error", text: "Por favor ingrese password"});
                    }
                    else
                    {
                        webix.message("Validando. Por favor espere...");
                        webix.ajax().post("social/google", payLoad, callbackMethod);
                    }
                }
                
                function resetForm() {
                    webix.message("Usuario y contraseña limpiados");
                    $$("theUserName").setValue("");
                    $$("thePassword").setValue("");
                }
                
                function callbackMethod(obj) {
                    if (obj == 'true')
                    {
                        window.location.href = '/home';
                    }
                    else
                    {
                        webix.message({type:"error", text: "Error de inicio de sesión, intentarlo nuevamente"});
                    }
                }

                var payLoad = {
                    userName : "",
                    password : "",
                    loginClicked : "",
                    resetClicked : ""
                };

                var theForm = webix.ui({
                    rows : [ {}, {
                        cols : [ {}, {
                            view :"form",
                            elements : [{
                                view: "text",
                                placeholder: "Nombre de usuario",
                                name: "theUserName"
                            },{
                                view: "text",
                                placeholder: "Contraseña",
                                type: "password",
                                name: "thePassword"
                            },{
                                view: "button",
                                label: "Login",
                                type: "form",
                                click: "validateForm"
                            },{
                                view: "button",
                                label: "Reset",
                                click: "resetForm",
                                name: "theResetButton"
                            },{
                                view: "button",
                                label: "Login Google",
                                click: "form",
                                name: "theGoogleLogin"
                            }]
                        }, {} ]
                    }, {} ]
                });

            </script>

        </form>

    </body>


{{--}}<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a href="{{ route('social.google') }}" class="btn btn-primary">
                                    Google
                                </a>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
@endsection
