<form class="form-horizontal" role="form" method="POST">
    @include('flash::message')
    {{ csrf_field() }}
    <script>
        function validateForm() {

            payLoad.userName =$$("theUserName").getValue();
            payLoad.password =$$("thePassword").getValue();
            payLoad.loginClicked = "Y";
            payLoad.resetClicked = "N";

            var localMessage = '';

            if(payLoad.userName == '')
            {
                webix.message({type:"error", text: "Por favor ingrese Nombre de usuario"});
            }
            else if(payLoad.password == '')
            {
                webix.message({type:"error", text: "Por favor ingrese Contraseña"});
            }
            else
            {
                webix.message("Validando. Por favor espere...");
                //window.location.href = 'home';
                webix.ajax().post("social/google", payLoad, callbackMethod);
            }
        }
        function googleLogin(){
            webix.message("Por favor espere...");
            window.location.href = '/social/google';
        }

        function resetForm() {
            webix.message("Usuario y contraseña limpiados");
            $$("theUserName").setValue("");
            $$("thePassword").setValue("");
        }

        function googlecallbackMethod(obj) {
            if (obj == 'true')
            {
                window.location.href = 'home';
            }
            else
            {
                webix.message({type:"error", text: "Error de inicio de sesión, intentarlo nuevamente"});
            }
        }

        function callbackMethod(obj) {
            if (obj == 'true')
            {
                window.location.href = '/home';
            }
            else
            {
                webix.message({type:"error", text: "Error de inicio de sesión, intentarlo nuevamente"});
            }
        }

        function modal () {
            webix.modalbox({
                title:"Custom title",
                buttons:["Yes", "No", "Maybe"],
                width:"500px",
                text:"Any html content here",
                callback: function(result){
                    switch(result){
                        case "0":
                            webix.alert("Aceptaste");
                            break;
                        case "1":
                            webix.alert("No Aceptaste");
                            break;
                        case "2":
                            webix.alert("Tal vez Aceptaste");
                            break;
                    }
                }
            });
        }

        var payLoad = {
            userName : "",
            password : "",
            loginClicked : "",
            resetClicked : ""
        };

        var languages = [
            {id:1, value:"English"},
            {id:2, value:"German"},
            {id:3, value:"Russian"},
            {id:4, value:"French"},
            {id:5, value:"Spanish"}
        ];

        webix.ui({
            rows:[
                {view:"tabbar", multiview:true, options:[
                    {id:"1", value:"{{ trans('lang.number.first') }} Tab"},
                    {id:"2", value:"{{ trans('lang.number.second') }} Tab"},
                    {id:"3", value:"{{ trans('lang.number.third') }} Tab"},
                    {id:"4", value:"{{ trans('lang.number.fourth') }} Tab"},
                    {id:"5", value:"{{ trans('lang.number.fifth') }} Tab"},
                    {id:"6", value:"{{ trans('lang.number.sixth') }} Tab"},
                    {id:"7", value:"{{ trans('lang.number.sixth') }} Tab"}
                ]
                },
                {cells:[
                    {   id:"1",

                        cols : [ {}, {
                            view :"form",
                            responsive:true,
                            elements : [
                                {view: "text",      placeholder: "{{ trans('lang.user_name') }}",   align:"center", id: "theUserName"},
                                {view: "text",      placeholder: "{{ trans('lang.password') }}",    align:"center", type: "password",    attributes:{maxlength:10}, id: "thePassword"},
                                {view: "button",    label: "{{ trans('lang.login_google') }}",      align:"center", click: "googleLogin",type: "danger"},
                                {view: "button",    label: "{{ trans('lang.reset') }}",             align:"center", click: "resetForm",  name: "theResetButton"}
                            ]
                        }, {} ]
                    },
                    { id:"2",

                        view:"datatable",
                        columns:[
                            { id:"title",   header:"Film title",  fillspace:true},
                            { id:"year",    header:"Year",      width:100},
                            { id:"votes",   header:"Votes",     width:100},
                            { id:"aaa",     header:"aaa",       width:100},
                            { id:"category",header:"Category",  width:100, editor:"select", options:["","Crime", "Thriller"]}
                        ],
                        data:[
                            { id:1, title:"The Shawshank Redemption", year:1994, votes:678790, aaa:111, category:""},
                            { id:2, title:"The Godfather",            year:1972, votes:511495, aaa:111, category:""},
                            { id:3, title:"The Godfather: Part II",   year:1974, votes:319352, aaa:111, category:""},
                            { id:4, title:"Pulp fiction",             year:1994, votes:533848, aaa:111, category:""},
                            { id:5, title:"Moonligth",                year:2016, votes:863349, aaa:111, category:""}

                        ],
                        editable:true,
                        select:"cell",
                        multiselect:false
                    },
                    {   id:"3",
                        cols : [ {}, {
                        type:"space",
                        view:"form", elements:[
                        {view: "text", id:"mytext", width:500, label:"First Name"},
                        {view: "text", name:"lname",id:"mytext2", width: 500, label:"Last Name" },
                        {view: "combo",id:"c1", options:["Female", "Male", "Any"], width:500, label:"Gender"},

                        {cols:[
                            {view: "button", id:"b1", value:"Click Me", type:"form", width:200 },
                            {view: "button", value:"Make First Name Wider", type:"form", click:"changeWidth()"}]
                        },
                        {cols:[
                            {view: "button", value:"Hide Last Name", type:"form", click:"$$('3').elements['lname'].hide()"},
                            {view: "button", value:"Show Last Name", type:"form", click:"$$('mytext2').show()"}]
                        }
                        ]
                        }, {} ]

                    },
                    { id:"4",
                        width:1370,
                        responsive:true,
                        type:"space",
                        view:"form", elements:[
                        {view:"text", id:"mytext1", align: "center", name:"fname", label:"First Name"},
                        {view:"text", id:"mytext2", align: "center", name:"lname", label:"Last Name"},
                        {view:"combo", id:"mycombo", align: "center", name:"lang", options:languages, label:"Language"},
                        {view:"button", type:"form", align: "center", value:"Set values"},
                        {view:"button", type:"form", align: "center", value:"GetValues", click:'get_values()'},
                        {view:"textarea", id:"log", align: "center", height:150}
                    ],
                        elementsConfig:{
                            inputWidth:500
                        }

                    },
                    { id:"5",

                        view:"select",
                        label:"Branch",
                        value:1, options:[
                            { "id":1, "value":"Master" },
                            { "id":2, "value":"Release" },
                            { "id":3, "value":"Option" }
                        ]


                    },
                    { id:"6",
                        width:1370,
                        view :"form",
                        elements : [
                            {view: "button", label: "Modal", align:"center", width:400, click: "modal", name: "modalButton"}
                        ]
                    },
                    {   id:"7",
                        rows: [
                            {type:"header", template:"Online Audio Player"},
                            {cols: [
                                {rows: [
                                    {template:"Tree"},
                                    {view:"resizer"},
                                    {template:"Album Art", width: 250, height: 250}
                                ]},
                                {view: "resizer"},
                                {rows: [
                                    {template:"Playlist", height: 440},
                                    {template:"Controls", height: 60}
                                ]}
                            ]
                            }
                        ]
                        /*margin:10, padding:0, type:"wide",
                        view:"flexlayout",
                        cols:[
                            { view:"list", data:[
                                { value:"Albert Brown" },{ value:"Mono Delorini" }
                            ], minWidth:320, select:true },
                            { view:"list", data:[
                                { value:"Accounts" },{ value:"History" },{ value:"Settings" }
                            ], minWidth:320, select:true },
                            {
                                template:"Try to resize screen, or open this sample on a mobile device",
                                minWidth:320, minHeight:400
                            }
                        ]*/
                    }
                ]
                }
            ]
        });
        function changeWidth(){
            $$("mytext").define("width", 300);
            $$("mytext").refresh();
        };

        $$("b1").attachEvent("onItemClick", function(){
            webix.alert("Funciona...!");
        });

        $$("c1").attachEvent("onChange", function(newv, oldv){
            webix.message("Value changed from: "+oldv+" to: "+newv);
        });
        $$("4").setValues({
            fname: "John",
            lname:"Clark",
            lang:"3"
        });

        function get_values(){
            var values = $$("4").getValues();
            values = JSON.stringify(values, null, 2);
            $$("log").setValue(values);

        };

    </script>
</form>
